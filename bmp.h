#ifndef BMP_H
#define BMP_H

struct bmp_header {
	uint16_t type;		/* Magic (BM) */
	uint32_t size;		/* File size in bytes */
	uint16_t res1;		/* Reserved 1 */
	uint16_t res2;		/* Reserved 2 */
	uint32_t offset;	/* Offset to image data in bytes */
} __attribute__ ((__packed__));
struct bmp_infoheader {
	uint32_t hdr_size;	/* the size of this header (40 bytes) */
	int32_t width;		/* the bitmap width in pixels */
	int32_t height;		/* the bitmap height in pixels */
	uint16_t planes;	/* the number of color planes (must be 1) */
	uint16_t bits;		/* the number of bits per pixel, which is the color depth of the image. Typical values are 1, 4, 8, 16, 24 and 32. */
	uint32_t compression;	/* the compression method being used. */
	uint32_t imagesize;	/* the image size. This is the size of the raw bitmap data; a dummy 0 can be given for BI_RGB bitmaps. */
	int32_t xresolution;	/* the horizontal resolution of the image. (pixel per meter, signed integer) */
	int32_t yresolution;	/* the veritical resolution of the image. (pixel per meter, signed integer) */
	uint32_t ncolours;	/* the number of colors in the color palette, or 0 to default to 2**n */
	uint32_t importantcolours;	/*the number of important colors used, or 0 when every color is important; generally ignored */
} __attribute__ ((__packed__));
struct bmp_palette {
	uint32_t red_mask;
	uint32_t green_mask;
	uint32_t blue_mask;
	uint32_t alpha_mask;

} __attribute__ ((__packed__));
struct bmp_custom {
	uint8_t offset_padding;	/* CUSTOM: Offset to padding (0-3) */
	uint8_t res3;		/* CUSTOM: Reserved 3 */
} __attribute__ ((__packed__));

struct bmp {
	struct bmp_header header;
	struct bmp_infoheader infoheader;
	struct bmp_palette palette;
	struct bmp_custom custom;
} __attribute__ ((__packed__));

extern int write_bmp_header(int fd);

extern int fget_real_size(int fd, uint32_t *size);
extern int get_real_size(const char *path, uint32_t *size);

#endif
