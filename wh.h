#ifndef WH_H
#define WH_H

extern void guess_width_height(uint32_t imagesize, uint32_t * width,
			       uint32_t * height);

#endif
