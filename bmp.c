#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include "wh.h"
#include "bmp.h"

int write_bmp_header(int fd)
{
	struct bmp hdr = { 0, };
	off_t size;
	uint16_t padding = 0;
	uint32_t width, height;
	char zero[3] = { 0, 0, 0 };

#ifndef BLOCKSIZE
	size = lseek(fd, 0, SEEK_END);
	if (size == -1)
		return -1;

	size -= sizeof(struct bmp);

	if (size % 4)
		padding = 4 - (size % 4);
#else
	static uint32_t fixed_width, fixed_height;

	size = BLOCKSIZE;

	if (fixed_width && fixed_height) {
		width = fixed_width;
		height = fixed_height;
	} else
#endif
	guess_width_height((size + padding) / 4, &width, &height);
#ifdef BLOCKSIZE
	fixed_width = width;
	fixed_height = height;
#endif

	hdr.header.type = 0x4d42,
	hdr.header.size = size + padding + sizeof(struct bmp),
	hdr.header.res1 = 0,
	hdr.header.res2 = 0,
	hdr.header.offset = sizeof(struct bmp) - sizeof(struct bmp_custom);
	hdr.infoheader.hdr_size = sizeof(struct bmp_infoheader) +
				  sizeof(struct bmp_palette);
	hdr.infoheader.width = width;
	hdr.infoheader.height = height;
	hdr.infoheader.planes = 1;
	hdr.infoheader.bits = 32;
	hdr.infoheader.compression = 3;
	hdr.infoheader.imagesize = size;
	hdr.infoheader.xresolution = 2835;
	hdr.infoheader.yresolution = 2835;
	hdr.infoheader.ncolours = 0;
	hdr.infoheader.importantcolours = 0;
	hdr.palette.red_mask =   0xff000000;
	hdr.palette.green_mask = 0x00ff0000;
	hdr.palette.blue_mask =  0x0000ff00;
	hdr.palette.alpha_mask = 0x000000ff;
	hdr.custom.offset_padding = padding;

	if (pwrite(fd, &hdr, sizeof(hdr), 0) == -1)
		return -1;
	if (padding)
		if (write(fd, &zero, padding) == -1)
			return -1;

	return 0;
}

int fget_real_size(int fd, uint32_t *size) {
#ifndef BLOCKSIZE
	struct bmp hdr;

	if (pread(fd, &hdr, sizeof(struct bmp), 0) == -1)
		return -1;

	*size = hdr.infoheader.imagesize;
#else
	(void) fd;
	*size = BLOCKSIZE;
#endif

	return 0;
}

int get_real_size(const char *path, uint32_t *size) {
#ifndef BLOCKSIZE
	int fd, res;

	fd = open(path, O_RDONLY);
	if (fd == -1)
		return -1;

	res = fget_real_size(fd, size);
	if (res == -1)
		return -1;

	res = close(fd);
	if (res == -1)
		return -1;
#else
	(void) path;
	*size = BLOCKSIZE;
#endif
	return 0;
}
