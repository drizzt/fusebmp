TARGET := fusebmp

OBJS := fusebmp.o bmp.o wh.o
DEPS := $(OBJS:.o=.d)

# FIXME
CPPFLAGS += -DHAVE_FDATASYNC -DHAVE_SETXATTR -D_HAVE_POSIX_FALLOCATE -DHAVE_UTIMENSAT

CPPFLAGS += -DBLOCKSIZE=4194304		# 4MiB, must be aligned to 4 and to cryfs blocksize

CPPFLAGS += $(INC_FLAGS) -MMD -MP
CPPFLAGS += $(shell pkg-config fuse --cflags-only-I)
CFLAGS += $(shell pkg-config fuse --cflags-only-other)
LDLIBS += $(shell pkg-config fuse --libs-only-l) -lulockmgr
LDFLAGS += -Wl,--as-needed $(shell pkg-config fuse --libs-only-L --libs-only-other)

all: $(TARGET)
$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o $@ $(LOADLIBES) $(LDLIBS)

.PHONY: clean
clean:
	$(RM) $(TARGET) $(OBJS) $(DEPS)

-include $(DEPS)
