#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "wh.h"

static uint64_t isqrt(uint64_t a)
{
	uint64_t rem = 0, root = 0;
	for (char i = 0; i < 16; i++) {
		root <<= 1;
		rem = ((rem << 2)) + (a >> 30);
		a <<= 2;
		root++;
		if (root <= rem) {
			rem -= root;
			root++;
		} else
			root--;
	}
	return (root >> 1);
}

void guess_width_height(uint32_t imagesize, uint32_t * width, uint32_t * height)
{
	int longt;
	uint32_t w, i = 0;

	if (imagesize > 11) {
		for (longt = 11; longt < 400; longt += 1) {
			for (w = isqrt(imagesize * 10 / longt);
			w < isqrt(imagesize * 10 * longt); w++) {
				if (imagesize % w == 0) {
					*width = w;
					*height = imagesize / w;
					return;
				}
			}
			i++;
		}
	}

	*width = 1;
	*height = imagesize;
	return;
}

#if 0
int main(int argc, char *argv[])
{
	uint64_t width, height;

	guess_width_height(strtoull(argv[1], NULL, 10), &width, &height);
	printf("%llux%llu\n", width, height);
}
#endif
